public abstract class Aviary<T> {
    private int currentSize;
    private Animal[] animals;

    public Aviary(int size) {
        currentSize = 0;
        animals = new Animal[size];
    }

    public void AddAnimal(Animal animal) throws Exception {
        if (!isGoodAnimal(animal)) {
            System.out.println(animal.toString() + " не возмжно засуниуть в вальер " + this.toString());
        }

        if (currentSize < animals.length) {
            animals[currentSize++] = animal;
        } else {
            System.out.println("Вальер запллнен");
        }
    }

    protected abstract boolean isGoodAnimal(Animal animal);

    public Animal[] GetAnimals() {
        return animals;
    }
}
